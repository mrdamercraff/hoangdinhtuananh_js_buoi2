/**
 * bai 1 :tinh luong
 */
var soNgayLamEl = document.getElementById('soNgayLam')

const luong1Ngay = 100000;

function tinhLuong(){
    var soLuong;
    soLuong = soNgayLamEl.value * luong1Ngay; 
    document.getElementById('soLuong').innerHTML = soLuong.toLocaleString() + ' VND'
}

/**
 * bài 2 : tính trung bình
 */



function tinhTrungBinh(){
    var so1 = document.getElementById('so1').value*1;
    var so2 = document.getElementById('so2').value*1;
    var so3 = document.getElementById('so3').value*1;
    var so4 = document.getElementById('so4').value*1;
    var so5 = document.getElementById('so5').value*1;
    var trungBinhCong;
    trungBinhCong = (so1 + so2 + so3 + so4 + so5) / 5;
    document.getElementById('trungBinhCong').innerHTML = trungBinhCong;
}


/**
 * bài 3 Tính tiền
 * 
 * 
 */
const giaTriQuyDoi = 23500;


function tinhTien(){
    var soDollar = document.getElementById('soDollar').value;
    var soTien;
    soTien = soDollar * giaTriQuyDoi;
    document.getElementById('soTien').innerHTML = soTien.toLocaleString() + ' VND';

}


/**
 * bài 4 tính diện tích chu vi
 */
function tinhDtCv(){
    var chieuDai = +document.getElementById('chieuDai').value;
    var chieuRong = +document.getElementById('chieuRong').value;
    var dienTich;
    var chuVi;
    dienTich = chieuDai * chieuRong;
    chuVi = (chieuDai + chieuRong) / 2;
    document.getElementById('DtCv').innerHTML = "Diện tích = " + dienTich + ", Chu vi =" + chuVi 


}

/**
 * bài 5 tính tổng 2 ký số
 */

function tinhKySo(){
    var soNguyen = +document.getElementById('soNguyen').value;
 
    var soHangDonVi = soNguyen%10;
    
    var soHangChuc = Math.floor(soNguyen/10)

    var tongKySo;
   
    tongKySo = soHangDonVi + soHangChuc;
   
    document.getElementById('tongKySo').innerHTML= tongKySo;
}